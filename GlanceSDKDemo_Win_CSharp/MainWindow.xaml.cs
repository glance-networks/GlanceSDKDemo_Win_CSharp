﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Glance;


namespace GlanceSDKDemo_Win_CSharp
{
     
    public partial class MainWindow : Window
    {
        public int GLANCE_GROUP_ID = 15687; // Get this from glance

        public bool RCEnabled = false; // Remote control for agent

        public MainWindow()
        {
            InitializeComponent();

            int ndisplays = HostSession.GetDisplayCount();

            // populating display menu with application window, and connected monitors
            MonitorSelect.Items.Add("Application");
            for (int i = 0; i <= ndisplays; i++)
            {
                if (HostSession.GetDisplayType(i) == DisplayType.Monitor)
                        MonitorSelect.Items.Add(HostSession.GetDisplayName(i));
            }

            Glance.Visitor.OnEvent += OnVisitorEvent;
            Glance.Visitor.Init(GLANCE_GROUP_ID, "", "Bro Nan", "bronan@glance.net", "+1 781-316-2569");

            // Add UIElements containing sensitive info
            Glance.Visitor.SetMaskedElements(new List<UIElement>() { MaskedField });
        }

        private void Start_Session(object sender, RoutedEventArgs e)
        {
            String toShow = MonitorSelect.Text;

            StartButton.Visibility = Visibility.Hidden;
            StopButton.Visibility = Visibility.Visible;

            if (toShow == "Application")
            {
                // default is to show the application window (and child windows)
                Glance.Visitor.StartSession();

                // this is the equivalent
                /*
                StartParams sp = new StartParams();
                sp.key = "GLANCE_KEYTYPE_RANDOM";
                IntPtr hwnd = new System.Windows.Interop.WindowInteropHelper(this).EnsureHandle();  // "this" is WPF MainWindow
                sp.displayParams.application.window = hwnd;
                sp.displayParams.displayName = "Window";
                Glance.Visitor.StartSession(sp);
                */
            }
            else
            {
                // otherwise initialize with start params including displayName
                StartParams sp = new StartParams();
                sp.key = "GLANCE_KEYTYPE_RANDOM";
                sp.displayParams.displayName = toShow;
                Glance.Visitor.StartSession(sp);
            }
        }

        private void End_Session(object sender, RoutedEventArgs e)
        {
            Console.WriteLine("Ending session");
            Glance.Visitor.EndSession();
        }

        private void ShowStatus(string status, string msg)
        {
            Status.Content = status + "\n" + msg;
        }

        private bool OnVisitorEvent(Glance.Event ge)
        {
            // Dispatch any UI updates to the main thread
            Dispatcher.Invoke((System.Action)delegate () {
                switch (ge.code)
                {
                    case Glance.EventCode.EventStartSessionFailed:
                        ShowStatus("Start Session Failed", ge.message);
                        StartButton.Visibility = Visibility.Visible;
                        StopButton.Visibility = Visibility.Hidden;
                        break;

                    case Glance.EventCode.EventConnectedToSession:
                        ShowStatus("Session Started", "Give this key to the agent: " + ge.properties["sessionkey"]);
                        break;

                    case Glance.EventCode.EventSessionEnded:
                        ShowStatus("Session Ended", ge.message);
                        StartButton.Visibility = Visibility.Visible;
                        StopButton.Visibility = Visibility.Hidden;
                        break;
                }

                // It is recommended to log any events of type Warning, Error or AssertFail
                if (ge.type == EventType.EventError || ge.type == EventType.EventError || ge.type == EventType.EventAssertFail)
                    Console.WriteLine("Glance.Visitor Event: {0}, {1}", ge.code, ge.message);
            });
            return true;
        }

        private void MonitorSelect_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            String selectedDisplay = MonitorSelect.SelectedValue.ToString();
            DisplayParams newScreen = new DisplayParams();

            // show application window: look up application window pointer, and set displayName to "Window"
            if (selectedDisplay == "Application")
            {
                IntPtr hwnd = new System.Windows.Interop.WindowInteropHelper(this).EnsureHandle();
                newScreen.application.window = hwnd;
                newScreen.displayName = "Window";
            }
            // show whole screen: just set the displayName to the monitor number
            else
            {
                newScreen.displayName = selectedDisplay;
            }

            Glance.Visitor.ShowDisplay(newScreen);
        }

        private void IdentifyDisplays(object sender, RoutedEventArgs e)
        {
            Glance.HostSession.IdentifyMonitors();
        }

        private void ToggleRC(object sender, RoutedEventArgs e)
        {
            this.RCEnabled = !this.RCEnabled;
            Glance.Visitor.EnableRC(this.RCEnabled);
        }
    }
}
