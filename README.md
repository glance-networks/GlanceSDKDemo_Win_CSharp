# Visitor SDK for Windows

This repo contains a sample application that Integrates our Screensharing SDK for Windows.  It is built with Windows Presentation Foundation (WPF) and C#

## Setup

Before you get started you'll need a Glance account.  

Contact Glance at mobilesupport@glance.net to set up a Group (organization account) and an individual Agent account.
You will receive:
1. A numeric Group ID
2. An agent username, for example agnes.example.glance.net (Glance usernames are in the form of DNS names)
3. An agent password

In production usage, agents will typically authenticate with a single-sign-on, often via their CRM implementation (e.g. Salesforce).

## Integration

## Quick start

Clone this project and replace the GLANCE_GROUP_ID value with your Group ID obtained from Glance.

## Installation

Download the Glance Windows SDK provided from Glance, or copy from this project.  

Copy Glance.dll (managed) and _Glance.dll (unmanaged) to your project.  Add a reference to the Glance.dll to your Visual Studio project.


### Initialization

```csharp
using Glance;
```

Initialize the SDK for your group only once.  You could do this in your constructor or on first usage.

```csharp
            Glance.Visitor.OnEvent += OnVisitorEvent;
            Glance.Visitor.Init(GLANCE_GROUP_ID, "", "Vera Visitor", "vv@glance.net", "+1 781-316-2569");
```

Replace `GLANCE_GROUP_ID` with your own numeric group id.  Empty strings or any value useful to you can be passed for name, email and phone.  Maximum character lengths are, name: 63, email: 127, phone: 31.
You should pass an empty string as the second argument to `Glance.Visitor.Init`, this parameter is reserved for future use.

Add the event handler:

```csharp
        private bool OnVisitorEvent(Glance.Event ge)
        {
            // Dispatch any UI updates to the main thread
            Dispatcher.Invoke((System.Action)delegate () {
                switch (ge.code)
                {
                    case Glance.EventCode.EventStartSessionFailed:
                        MessageBox.Show(ge.message, "Start Session Failed");
                        break;
                    case Glance.EventCode.EventConnectedToSession:
                        MessageBox.Show("session key:" + ge.properties["sessionkey"], "Session Started") ;
                        break;
                    case Glance.EventCode.EventSessionEnded:
                        MessageBox.Show(ge.message, "Session Ended");
                        break;
                }

                // It is recommended to log any events of type Warning, Error or AssertFail
                if (ge.type == EventType.EventError || ge.type == EventType.EventError || ge.type == EventType.EventAssertFail)
                    Console.WriteLine("Glance.Visitor Event: {0}, {1}", ge.code, ge.message);
            });
            return true;
        }
```

### Starting a Session

There are two ways to start a session: with and without a session key.  If no session key is provided then one will be generated and returned in the properties of the GlanceEvent with code EventConnectedToSession.

To start a session with a random session key:

```csharp
            Glance.Visitor.StartSession();
```

To start a session with a specified session key:

```csharp
            Glance.Visitor.StartSession("123456");
```

### Stopping a Session

To stop a visitor session in progress make the following call:

```csharp
            Glance.Visitor.EndSession();
```

### Masking

Fields with personal private information or banking information should be hidden from the agent.  You can mask UIElements like this:

```csharp
            Glance.Visitor.SetMaskedElements(new List<UIElement>() { txt_Password, txt_SSNumber });
```

#### Changing the Masking Color

Remember, only the agent (screenshare viewer) sees the masking.

On Windows the masking color is set in the Registry in: `HKEY_CURRENT_USER\Software\GlanceNetworks\Glance\DMaskingColor`

The value is hex bbggrr, the default value is ffbf80.

### Showing a Window or Application
The `StartSession` method by default will show your application's main window and its child windows.  To show the entire screen or other applications or windows pass a `StartParams` object to `StartSession`.  To change what is shown during a session, pass a `DisplayParams` object to `Glance.Visitor.ShowDisplay`.

Note that it is only possible to mask UIElements that belong to your own application.

#### Showing an Entire Screen

To show the main display screen:

```csharp
    StartParams sp = new StartParams();

    sp.key = "123456";   // or use "GLANCE_KEYTYPE_RANDOM" to generate a random key
    sp.displayParams.displayName = "Main";
    Glance.Visitor.StartSession(sp);
```

#### Showing a Window
The SDK can be used to show a top-level application window.  The window is identified by its Windows API window handle (HWND).

The window does not need to belong to your application, you could for example use `FindWindow` to look for another application’s window by title or class and and pass the returned HWND.

The HWND is passed in `StartParams.displayParams.application.window`.  You must also set the `displayParams.displayName` to the string “Window”.  

Here is an example in C# that can be used from a WPF Window method to show that window:

```csharp
    StartParams sp = new StartParams();

    sp.key = "123456";   // or use "GLANCE_KEYTYPE_RANDOM" to generate a random key
    IntPtr hwnd = new System.Windows.Interop.WindowInteropHelper(this).EnsureHandle();
    sp.displayParams.application.window = hwnd;
    sp.displayParams.displayName = "Window";
    Glance.Visitor.StartSession(sp);
```

A different window can be shown on the same session by calling ShowDisplay on the Visitor object.

Glance will show one top-level window and its associated [Child](https://docs.microsoft.com/en-us/windows/win32/winmsg/window-features?redirectedfrom=MSDN#child-windows) and [Owned](https://docs.microsoft.com/en-us/windows/win32/winmsg/window-features?redirectedfrom=MSDN#owned-windows) windows.

If your application opens other top-level windows that you wish to show, the options are:
- You could make the new window owned by the original shown window
- On showing or activating a window you can call ShowDisplay again to show that window.
- You could show your application by name as described below


### Showing Applications

A list of named applications can also be shown:

```csharp
StartParams sp = new StartParams();

sp.displayParams.displayName = "Applications";
sp.displayParams.application.names = "Chrome.exe|Notepad.exe";
```

Note the following security considerations:

1. All windows of an application will be shown, so the example above would show all Chrome browser windows.
2. It would be possible for a user with sufficient privileges to rename any application to one on the application list and show it.

### Gesturing

Gesturing allows the viewer on a session to indicate areas on the shown screen.  The viewer’s mouse pointer will be shown.  Mouse clicks will show a “radiating circle” effect, and click-drag will draw a rectangle.

Gesturing is enabled and disabled on a session by calling HostSession.InvokeAction with ActionEnableGestures or ActionDisableGestures:

```csharp
    if (session != null)
        session.InvokeAction(Glance.Action.ActionEnableGestures);
```

Currently gesturing is enabled by default.  The actions can be passed to CanInvokeAction to determine if enabling or disabling is allowed in the current state or with the current account privileges.

The gesture color can be set in the registry setting:

`HKEY_CURRENT_USER\Software\GlanceNetworks\Glance\DrawingColor`
This a 24-bit BGR value 0xbbggrr.  The default is 0069f9.

Note that gesturing and remote control are mutually exclusive.

### Application Manifest

#### DPI Awareness

In order to properly handle Windows 10 per-monitor DPI scaling in the Glance SDK, particularly with masking and gesturing, the application manifest must be configured correctly:

In the `app.manifest` file `dpiAwareness` should be set to `PerMonitorV2`.  You can insert the entire `windowsSettings` element anywhere inside the <assembly> node:

```
<windowsSettings>
     <dpiAware xmlns="http://schemas.microsoft.com/SMI/2005/WindowsSettings">true</dpiAware>
     <dpiAwareness xmlns="http://schemas.microsoft.com/SMI/2016/WindowsSettings">PerMonitorV2</dpiAwareness>
</windowsSettings>
```

Or, uncomment the one that is there and add the line:
 ```
<dpiAwareness xmlns="http://schemas.microsoft.com/SMI/2016/WindowsSettings">PerMonitorV2</dpiAwareness>  
```

To add an app.manifest in Visual Studio, select the project and from the right mouse button menu select "Add..." > "New Item" and then select Application Manifest from the list.

- See this for more details on adding the app.manifest https://stackoverflow.com/questions/6050478/how-do-i-create-edit-a-manifest-file
- See this document for more information on the application manifest: https://docs.microsoft.com/en-us/windows/desktop/sbscs/application-manifests

### UIAccess

Some Windows applications -- for example Regedit -- need to run with User Account Control (UAC) elevation.  If you wish to screenshare these applications using the Glance SDK, your application must set uiAccess="true" in requestedExecutionLevel in the app manifest:
```
  <trustInfo xmlns="urn:schemas-microsoft-com:asm.v2">
    <security>
      <requestedPrivileges xmlns="urn:schemas-microsoft-com:asm.v3">
        <requestedExecutionLevel level="asInvoker" uiAccess="true" />
      </requestedPrivileges>
    </security>
  </trustInfo>
```

## Testing

### Confirm it's working

To confirm the integration is working, start the session and note the session key.

The agent should go to:
<https://www.glance.net/agentjoin> then login with their Glance username and password.  A form will be shown to enter the session key.

When the key is known a view can be opened directly with
<https://www.glance.net/agentjoin/AgentView.aspx?username={username}&sesionkey={key}&wait=1>

See [Glance Documentation](https://help.glance.net/integrations/) for details on integrating the agent viewer with other systems and single-sign-on
